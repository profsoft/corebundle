<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class UniqueFieldsForEntityModel
 *
 * @Annotation
 */
class UniqueFieldsForEntityModel extends UniqueEntity
{
    /**
     * @var mixed
     */
    public $class = null;

    /**
     * @var mixed
     */
    public $entity = null;

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'uniqueFieldsEntityModelValidator';
    }
}
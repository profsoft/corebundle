<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class LengthEachWord
 *
 * @Annotation
 */
class LengthEachWord extends Constraint
{
    /**
     * @var int
     */
    public $length = 15;

    /**
     * @var string
     */
    public $message = 'validation.acceptable_values_word_length';

    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'length';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return array('length');
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'lengthEachWordValidator';
    }
}

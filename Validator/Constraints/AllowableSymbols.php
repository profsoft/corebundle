<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class AllowableSymbols
 *
 * @Annotation
 */
class AllowableSymbols extends Regex
{
    public $patternType;

    public $length = array();

    public function __construct($options)
    {
        parent::__construct($options);

        switch ($this->patternType) {
            case 'name':
                $this->pattern = "/^[А-Яа-яЧчЁёЦцУуШшЩщХхЪъФфЫыРрЭэСсТтЬьЮю-]+$/";
                $this->message = "validation.acceptable_values_name";

                break;

            case 'composite-name':
                $this->pattern = "/^[А-Яа-яЧчЁёЦцУуШшЩщХхЪъФфЫыРрЭэСсТтЬьЮю\s-]+$/";
                $this->message = "validation.acceptable_values_composite_name";

                break;

            case 'password':
                $this->pattern = "/^[a-zA-Z\d_-]+$/";
                $this->message = "validation.acceptable_values_password";

                break;

            case 'integer':
                if (array_key_exists('length', $options) && is_array($options['length'])) {
                    $this->pattern = "/";

                    foreach ($options['length'] as $key => $length) {
                        $length = intval($length);

                        $or = ($key < count($options['length']) - 1) ? '|' : '';

                        $this->pattern .= "(^[\d]{{$length}}+$){$or}";
                    }

                    $this->pattern .= "/";
                    $this->message = "validation.acceptable_values_integer_length";
                } else {
                    $this->pattern = "/^[\d]+$/";
                    $this->message = "validation.acceptable_values_integer";
                }

                break;

            case 'phone-number':
                $this->pattern = "/^[\d\s\(\)+-]+$/";
                $this->message = "validation.acceptable_values_phone_number";

                break;

            case 'inn':
                $this->pattern = "/(^[\d]{10}+$)|(^[\d]{12}+$)/";
                $this->message = "validation.acceptable_values_inn";

                break;

            case 'article':
                $this->pattern = "/^[a-zA-ZА-Яа-яЧчЁёЦцУуШшЩщХхЪъФфЫыРрЭэСсТтЬьЮю\d_-]+$/";
                $this->message = "validation.acceptable_values_article";

                break;

            case 'city':
                $this->pattern = "/^[А-Яа-яЧчЁёЦцУуШшЩщХхЪъФфЫыРрЭэСсТтЬьЮю\s-.]+$/";
                $this->message = "validation.acceptable_values_city";

                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'patternType';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return array('patternType');
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'allowableSymbolsValidator';
    }
}
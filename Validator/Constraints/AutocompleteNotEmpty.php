<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class AutocompleteNotEmpty
 *
 * @Annotation
 */
class AutocompleteNotEmpty extends Constraint
{
    public $message = 'This value should not be blank.';
    public $idProperty = 'id';
    public $property = 'title';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'autocompleteNotEmptyValidator';
    }
}

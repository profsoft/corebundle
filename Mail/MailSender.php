<?php
namespace Rup\Bundle\CoreBundle\Mail;

use Rup\Bundle\CoreBundle\Mail\Adapter\MailAdapter;

/**
 * Class MailSender
 */
class MailSender
{
    /**
     * @var MailAdapter
     */
    protected $mailer;

    /**
     * @param MailAdapter $mailer
     */
    public function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param MailContext $context
     */
    public function send($context)
    {
        $this->mailer->send($context);
    }
}

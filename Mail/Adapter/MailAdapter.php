<?php
namespace Rup\Bundle\CoreBundle\Mail\Adapter;

use Rup\Bundle\CoreBundle\Mail\MailContext;

/**
 * Interface MailAdapter
 */
interface MailAdapter
{
    /**
     * @param MailContext $context
     *
     * @return mixed
     */
    public function send($context);
}

<?php
namespace Rup\Bundle\CoreBundle\Mail\Adapter;

use Rup\Bundle\CoreBundle\Mail\MailContext;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SwiftMailAdapter
 */
class SwiftMailAdapter implements MailAdapter
{

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var EngineInterface
     */
    protected $renderer;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param Swift_Mailer        $mailer
     * @param EngineInterface     $renderer
     * @param TranslatorInterface $translator
     */
    public function __construct($mailer, $renderer, $translator)
    {
        $this->mailer     = $mailer;
        $this->renderer   = $renderer;
        $this->translator = $translator;
    }

    /**
     * @param MailContext $context
     *
     * @return mixed
     */
    public function send($context)
    {
        $mail = Swift_Message::newInstance();

        $mail->setSubject($this->translator->trans($context->subject))
            ->setFrom($context->from)
            ->setTo($context->to)
            ->setBody(
                $this->renderer->render($context->bodyTemplate, $context->bodyTemplateParams),
                $context->contentType);

        $this->mailer->send($mail);
    }
}

<?php

namespace Rup\Bundle\CoreBundle\Mail;

/**
 * Class MailContext
 */
class MailContext
{
    /**
     * @var string
     */
    public $subject;

    /**
     * @var string
     */
    public $bodyTemplate;

    /**
     * @var array
     */
    public $bodyTemplateParams;

    /**
     * @var array|string
     */
    public $to;

    /**
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $contentType = 'text/html';
}

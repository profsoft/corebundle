<?php
namespace Rup\Bundle\CoreBundle\Repository\Published;

use Doctrine\ORM\QueryBuilder;

/**
 * Class PublishedTrait
 *
 * @package Rup\Bundle\CoreBundle\Repository\Published
 */
trait PublishedTrait
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field
     */
    public function addPublished(QueryBuilder $queryBuilder, $field)
    {
        $queryBuilder->andWhere("{$field} = 1");
    }
} 
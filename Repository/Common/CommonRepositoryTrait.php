<?php
namespace Rup\Bundle\CoreBundle\Repository\Common;

use Rup\Bundle\CoreBundle\Exception\Type\TypeException;
use Rup\Bundle\CoreBundle\Utils\StringUtils;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CommonRepositoryTrait
 *
 * @package Rup\Bundle\CoreBundle\Repository\Common
 */
trait CommonRepositoryTrait
{
    /**
     * Example:
     * <code>
     *     $this->addFieldTerm(
     *         $qb = $em->createQueryBuilder('u'),
     *         array('u.firstName', 'u.lastName', 'u.middleName'),
     *         'John Doe',
     *         true,
     *         ' ',
     *         10
     *     );
     * </code>
     * @param QueryBuilder $qb
     * @param string|array $fields should be defined in format "alias.fieldName"
     * @param string $value
     * @param bool $anyMatching
     * @param string $explodeDelimiter
     * @param int $explodeLimit
     *
     * @throws TypeException
     */
    public function addFieldTerm(QueryBuilder $qb, $fields, $value, $anyMatching = false, $explodeDelimiter = ' ', $explodeLimit = 3)
    {
        if (is_string($fields)) {
            $fields = array($fields);
        }

        $words = StringUtils::splitWords($value, $explodeDelimiter, $explodeLimit);
        $orWhere = $qb->expr()->orX();

        foreach ($fields as $field) {
            $andWhere = $qb->expr()->andX();

            foreach ($words as $wordIndex => $word) {
                $parameter = $this->createParameterName($field, $wordIndex);

                $like = $this->createLike($qb, $field, $word, $parameter, $anyMatching);

                $andWhere->add($like);
            }

            $orWhere->add($andWhere);
        }

        $qb->andWhere($orWhere);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addIn(QueryBuilder $queryBuilder, $field, $values)
    {
        if (is_array($values) && count($values)) {
            $values = $queryBuilder->expr()->in($field, $values);
            $queryBuilder->andWhere($values);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addNotIn(QueryBuilder $queryBuilder, $field, $values)
    {
        if (is_array($values) && count($values)) {
            $values = $queryBuilder->expr()->notIn($field, $values);
            $queryBuilder->andWhere($values);
        }
    }

    /**
     * Add types/status select
     *
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $types
     */
    public function addTypes($queryBuilder, $field, $types)
    {
        $orExpr = $queryBuilder->expr()->orX();

        foreach ($types as $type) {
            $parameter = ':type_' . $type;

            $orExpr->add("$field = $parameter");
            $queryBuilder->setParameter($parameter, $type);
        }

        $queryBuilder->andWhere($orExpr);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param int $rangeFrom
     * @param int $rangeTo
     */
    public function addRange(QueryBuilder $queryBuilder, $field, $rangeFrom, $rangeTo)
    {
        $range = $queryBuilder->expr()->between($field, $rangeFrom, $rangeTo);

        $queryBuilder->andWhere($range);
    }

    /**
     * @param QueryBuilder $qb
     * @param string $field
     * @param string $value
     * @param string $parameterName
     * @param bool $anyMatching
     *
     * @throws TypeException
     *
     * @return Comparison
     */
    protected function createLike(QueryBuilder $qb, $field, $value, $parameterName = null, $anyMatching = false)
    {
        if (!is_string($field) || !is_string($field)) {
            throw new TypeException(sprintf("Given field or value is not string in %s", __METHOD__));
        }

        if (!$parameterName) {
            $parameterName = $this->createParameterName($field);
        }

        $like = $qb->expr()->like($field, ":$parameterName");

        $value = ($anyMatching) ? '%' . $value . '%' : $value . '%';

        $qb->setParameter($parameterName, $value);

        return $like;
    }

    /**
     * @param $field
     * @param null $fieldSalt
     *
     * @return string
     */
    protected function createParameterName($field, $fieldSalt = null)
    {
        return 'field_' . str_replace('.', '_', $field) . '_' . $fieldSalt;
    }
}
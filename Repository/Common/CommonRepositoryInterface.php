<?php
namespace Rup\Bundle\CoreBundle\Repository\Common;

use Doctrine\ORM\QueryBuilder;

/**
 * Interface CommonRepositoryInterface
 *
 * @package Rup\Bundle\CoreBundle\Repository\Common
 */
interface CommonRepositoryInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param string $term
     */
    public function addFieldTerm(QueryBuilder $queryBuilder, $field, $term);

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addIn(QueryBuilder $queryBuilder, $field, $values);

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addNotIn(QueryBuilder $queryBuilder, $field, $values);

    /**
     * Add types/status select
     *
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $types
     */
    public function addTypes($queryBuilder, $field, $types);

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $field should be defined in format "alias.fieldName"
     * @param int $rangeFrom
     * @param int $rangeTo
     */
    public function addRange(QueryBuilder $queryBuilder, $field, $rangeFrom, $rangeTo);
}
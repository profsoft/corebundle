<?php
namespace Rup\Bundle\CoreBundle\Handler;

/**
 * Interface UpdateDomainObjectInterface
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
interface UpdateDomainObjectInterface
{
    /**
     * @param object $entity
     * @param object $entityDto
     */
    public function update($entity, $entityDto);
} 
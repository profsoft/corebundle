<?php
namespace Rup\Bundle\CoreBundle\Handler;

/**
 * Interface CreateDomainObjectInterface
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
interface CreateDomainObjectInterface
{
    /**
     * @param object $entity
     * @param object $entityDto
     */
    public function create($entity, $entityDto);
} 
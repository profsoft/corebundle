<?php
namespace Rup\Bundle\CoreBundle\Handler;

/**
 * Interface PublishHandlerInterface
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
interface PublishHandlerInterface
{
    /**
     * @param object $entity
     *
     * @return bool
     */
    public function publish($entity);

    /**
     * @param object $entity
     *
     * @return bool
     */
    public function cancelPublish($entity);
} 
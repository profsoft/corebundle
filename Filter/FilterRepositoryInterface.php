<?php
namespace Rup\Bundle\CoreBundle\Filter;

use Porpaginas\Result;

/**
 * Interface FilterRepositoryInterface
 *
 * @package Rup\Bundle\CoreBundle\Filter
 */
interface FilterRepositoryInterface
{
    /**
     * @param FilterInterface $filter
     *
     * @return Result
     */
    public function findByFilter($filter);
}

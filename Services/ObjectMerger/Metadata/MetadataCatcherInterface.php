<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata;

/**
 * Interface MetadataCatcherInterface
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata
 */
interface MetadataCatcherInterface
{
    /**
     * @param mixed $object
     *
     * @return array
     */
    public function getProperties($object);
}

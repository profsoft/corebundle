<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata;

use Doctrine\Common\Util\ClassUtils;
use JMS\Serializer\Context;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\PropertyMetadata;
use Metadata\MetadataFactory;

/**
 * Class JMSSerializerMetadataCatcher
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata
 */
class JMSSerializerMetadataCatcher implements MetadataCatcherInterface
{
    /**
     * @var MetadataFactory
     */
    protected $metadataFactory;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @param MetadataFactory $metadataFactory
     * @param Context         $context
     */
    public function __construct(
        MetadataFactory $metadataFactory,
        $context = null
    )
    {
        $this->metadataFactory = $metadataFactory;
        $this->setContext($context);
    }

    /**
     * @return Context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param Context $context
     *
     * @return JMSSerializerMetadataCatcher
     */
    public function setContext($context)
    {
        if ($context instanceof Context) {
            $this->context = $context;

            return $this;
        }

        $this->context = new DeserializationContext();

        if (is_array($context)) {
            $this->context->setGroups($context);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getProperties($object)
    {
        $exclusionStrategy = $this->context->getExclusionStrategy();
        $entityClass       = ClassUtils::getRealClass(get_class($object));
        $metadata          = $this->metadataFactory->getMetadataForClass($entityClass);

        $properties = array();
        /** @var PropertyMetadata $property */
        foreach ($metadata->propertyMetadata as $property) {
            if (null !== $exclusionStrategy && $exclusionStrategy->shouldSkipProperty($property, $this->context)) {
                continue;
            }

            if ($this->context instanceof DeserializationContext && $property->readOnly) {
                continue;
            }

            $properties[] = $property->name;
        }

        return $properties;
    }
}

<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityInterface;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityRepositoryInterface;
use Rup\Bundle\CoreBundle\Model\TitledEntity;
use Rup\Bundle\CoreBundle\Repository\EntityRepository;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata\JMSSerializerMetadataCatcher;
use Rup\Bundle\CoreBundle\Utils\ArrayCache;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class ExternalEntityMerger
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger
 */
class ExternalEntityMerger extends BaseMerger
{
    /**
     * @var JMSSerializerMetadataCatcher
     */
    protected $metadataCatcher;

    /**
     * @param EntityManager                $entityManager
     * @param JMSSerializerMetadataCatcher $metadataCatcher
     */
    public function __construct(
        EntityManager $entityManager,
        JMSSerializerMetadataCatcher $metadataCatcher
    )
    {
        parent::__construct($entityManager, $metadataCatcher);
        $this->cache = new ArrayCache('externalId');
    }

    /**
     * @param TitledEntity|ExternalEntityInterface $value TitledEntity interface for ide autocomplete
     *
     * @return ExternalEntityInterface
     * @throws EntityNotFoundException
     */
    protected function fetchManagedObject($value)
    {
        /** @var EntityRepository|ExternalEntityRepositoryInterface $repository */
        $repository = $this->objectManager->getRepository(get_class($value));

        if ($value instanceof ExternalEntityInterface) {
            $entity = null;
            try {
                return $repository->findOneByExternalId($value->getExternalId());
            } catch (Exception $e) {}
        }

        try {
            return $repository->find($value->getId());
        } catch (Exception $e) {}

        return null;
    }
}

<?php
namespace  Rup\Bundle\CoreBundle\Services\Override\JMSSerializer\Construction;

use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\VisitorInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\DeserializationContext;

/**
 * Class ObjectEmptyConstructor
 *
 * @package Rup\Bundle\CoreBundle\Services\Override\JMSSerializer\Construction
 */
class ObjectEmptyConstructor implements ObjectConstructorInterface
{
    public function construct(VisitorInterface $visitor, ClassMetadata $metadata, $data, array $type, DeserializationContext $context)
    {
        return new $metadata->name;
    }
}

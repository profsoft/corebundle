<?php
namespace Rup\Bundle\CoreBundle\Services\Override\JMSSerializer;

use JMS\Serializer\Context;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\JsonDeserializationVisitor as BaseJsonDeserializationVisitor;
use JMS\Serializer\Metadata\PropertyMetadata;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class GenericDeserializationVisitor
 *
 * @package Rup\Bundle\CoreBundle\Services\Override\JMSSerializer
 */
class JsonDeserializationVisitor extends BaseJsonDeserializationVisitor
{
    /**
     * @param PropertyMetadata $metadata
     * @param mixed            $data
     * @param Context          $context
     */
    public function visitProperty(PropertyMetadata $metadata, $data, Context $context)
    {
        $name = $this->namingStrategy->translateName($metadata);

        if (null === $data || !array_key_exists($name, $data)) {
            return;
        }

        if (!$metadata->type) {
            throw new RuntimeException(sprintf('You must define a type for %s::$%s.', $metadata->reflection->class, $metadata->name));
        }

        $v = $data[$name] !== null ? $this->getNavigator()->accept($data[$name], $metadata->type, $context) : null;

        $currentObject = $this->getCurrentObject();

        if (null === $metadata->setter) {
            // Reflection setter replaced with PropertyAccess
            $accessor = PropertyAccess::createPropertyAccessor();
            $accessor->setValue($currentObject, $metadata->reflection->getName(), $v);

            return;
        }

        $currentObject->{$metadata->setter}($v);
    }
}

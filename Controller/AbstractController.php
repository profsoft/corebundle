<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Rup\Bundle\CoreBundle\Model\TitledEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AbstractController
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
abstract class AbstractController extends Controller
{
    /**
     * @return EntityRepository
     */
    abstract protected function getRepository();

    /**
     * @param int $id
     *
     * @return null|object
     *
     * @throws NotFoundHttpException
     */
    protected function findEntity($id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException();
        }

        return $entity;
    }

    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string      $id         The message id (may also be an object that can be cast to string)
     * @param array       $parameters An array of parameters for the message
     * @param string|null $domain     The domain for the message or null to use the default
     * @param string|null $locale     The locale or null to use the default
     *
     * @throws \InvalidArgumentException If the locale contains invalid characters
     *
     * @return string The translated string
     */
    protected function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        $translator = $this->get('translator');

        return $translator->trans($id, $parameters, $domain, $locale);
    }

    /**
     * Checks if request is ajax
     *
     * @param Request $request
     *
     * @return bool
     *
     * @throws NotFoundHttpException
     */
    protected function isAjax(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        return true;
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param array|string $attributes The attributes
     * @param mixed        $object     The object
     * @param string|null  $message
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->container->getParameter('controller.grant.check')) {
            return;
        }

        if (!is_array($attributes)) {
            $attributes = array($attributes);
        }

        if ('dev' === $this->container->getParameter('kernel.environment')) {
            $attributesString = array_reduce($attributes, function ($result, $item) {
                if (!$result) {
                    $result = $item;
                } else {
                    $result .= ', ' . $item;
                }

                return $result;
            });

            $message .= ' Permissions: [' . $attributesString . '] are not allowed.';
        }

        parent::denyAccessUnlessGranted($attributes, $object, $message);
    }

    /**
     * Gets a parameter.
     *
     * @param string $name The parameter name
     *
     * @return mixed The parameter value
     */
    protected function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    /**
     * @param TitledEntity[]|Collection $entities
     *
     * @return array
     */
    protected function entitiesToArrays($entities)
    {
        $entityArray = array();

        if (is_array($entities) && count($entities)) {
            foreach ($entities as $entity) {
                if ($entity instanceof TitledEntity) {
                    $entityArray[] = array(
                        'id'    => $entity->getId(),
                        'title' => $entity->getTitle()
                    );
                }
            }
        }

        return $entityArray;
    }

    /**
     * @return FlashBagInterface|SessionBagInterface
     */
    protected function getMessenger()
    {
        return $this->get('session')->getFlashBag();
    }

    /**
     * @param string $text
     */
    protected function addDangerMessage($text)
    {
        $this->addFlash('danger', $text);
    }

    /**
     * @param string $text
     */
    protected function addSuccessMessage($text)
    {
        $this->addFlash('success', $text);
    }
}

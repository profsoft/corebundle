<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Rup\Bundle\CoreBundle\Dto\CreateDtoInterface;
use Rup\Bundle\CoreBundle\Form\CrudFormHandlerInterface;
use Rup\Bundle\CoreBundle\Handler\CreateDomainObjectInterface;
use Rup\Bundle\CoreBundle\Handler\UpdateDomainObjectInterface;
use Rup\Bundle\CoreBundle\Model\Paginator\PaginatorInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AbstractCrudController
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
abstract class AbstractCrudController extends Controller implements PaginationControllerInterface
{
    use PaginationControllerTrait;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @var string
     */
    protected $twigFileNamespace;

    /**
     * @var string
     */
    protected $redirectUrlNamespace;

    /**
     * @return CreateDtoInterface
     */
    abstract protected function getCreateDtoHandler();

    /**
     * @return CreateDomainObjectInterface
     */
    abstract protected function getCreateDomainObjectHandler();

    /**
     * @return UpdateDomainObjectInterface
     */
    abstract protected function getUpdateDomainObjectHandler();

    /**
     * @return CrudFormHandlerInterface
     */
    abstract protected function getFormHandler();

    /**
     * @return string
     */
    protected function getTwigFileNamespace()
    {
        return $this->twigFileNamespace;
    }

    /**
     * @return string
     */
    protected function getRedirectUrlNamespace()
    {
        return $this->redirectUrlNamespace;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request)
    {
        $entities   = $this->getRepository()->findAllForPaginator();
        $pagination = $this->getPagination($entities);

        return $this->render(
            $this->getTwigFileNamespace() . ':list.html.twig',
            array(
                'entities'      => $pagination,
                'previousIndex' => $this->getNumberOfPreviousItemsForPagination($pagination)
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function viewAction(Request $request)
    {
        $id = $request->get('id');

        $entity    = $this->findEntity($id);
        $entityDto = $this->getCreateDtoHandler()->createDto($entity);

        return $this->render(
            $this->getTwigFileNamespace() . ':view.html.twig',
            array(
                'entityDto' => $entityDto
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $entityName = $this->getRepository()->getClassName();
        $entity     = new $entityName();

        $entityDto = $this->getCreateDtoHandler()->createDto($entity);

        $form = $this->createAddForm($entityDto);

        if ($request->getMethod() == 'POST') {

            $form->submit($request);

            if ($form->isValid()) {

                $this->getCreateDomainObjectHandler()->create($entity, $entityDto);
                $this->getManager()->flush();

                return $this->afterAdd($entity);
            }
        }

        return $this->render(
            $this->getTwigFileNamespace() . ':add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request)
    {
        $id = $request->get('id');

        $entity = $this->findEntity($id);

        $entityDto = $this->getCreateDtoHandler()->createDto($entity);

        $form = $this->createEditForm($entityDto);

        if ($request->getMethod() == 'POST') {

            $form->submit($request);

            if ($form->isValid()) {

                $this->getUpdateDomainObjectHandler()->update($entity, $entityDto);
                $this->getManager()->flush();

                return $this->afterEdit($entity);
            }
        }

        return $this->render(
            $this->getTwigFileNamespace() . ':edit.html.twig',
            array(
                'form' => $form->createView(),
                'entity' => $entity
            )
        );
    }

    /**
     * @param $entity
     *
     * @return Form
     */
    protected function createAddForm($entity)
    {
        return $this->getFormHandler()->getAddForm($entity);
    }

    /**
     * @param $entity
     *
     * @return Form
     */
    protected function createEditForm($entity)
    {
        return $this->getFormHandler()->getEditForm($entity);
    }

    /**
     * @param int $id
     *
     * @return null|object
     *
     * @throws NotFoundHttpException
     */
    protected function findEntity($id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException();
        }

        return $entity;
    }

    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return EntityRepository|PaginatorInterface
     */
    protected function getRepository()
    {
        return $this->get($this->repositoryName);
    }

    /**
     * @param object $entity
     *
     * @return RedirectResponse
     */
    protected function afterAdd($entity)
    {
        return $this->redirect(
            $this->generateUrl($this->getRedirectUrlNamespace() . '_view', array('id' => $entity->getId()))
        );
    }

    /**
     * @param object $entity
     *
     * @return RedirectResponse
     */
    protected function afterEdit($entity)
    {
        return $this->redirect(
            $this->generateUrl($this->getRedirectUrlNamespace() . '_view', array('id' => $entity->getId()))
        );
    }

    /**
     * @return RedirectResponse
     */
    protected function afterDelete()
    {
        return $this->redirect($this->generateUrl($this->getRedirectUrlNamespace() . '_list'));
    }

    /**
     * @param string      $id         The message id (may also be an object that can be cast to string)
     * @param array       $parameters An array of parameters for the message
     * @param string|null $domain     The domain for the message or null to use the default
     * @param string|null $locale     The locale or null to use the default
     *
     * @throws \InvalidArgumentException If the locale contains invalid characters
     *
     * @return string The translated string
     */
    protected function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        $translator = $this->get('translator');

        return $translator->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @param Request $request
     *
     * @return bool
     *
     * @throws NotFoundHttpException
     */
    protected function isAjax(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        return true;
    }
}

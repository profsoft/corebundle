<?php

namespace Rup\Bundle\CoreBundle\Menu;

use Rup\Bundle\CoreBundle\Exception\NotFoundException;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MenuParser
 */
class MenuParser
{
    const TAG_ITEMS = 'items';

    /**
     * @var string
     */
    protected $configFile;

    /**
     * @var string|boolean
     */
    protected $mapping;

    /**
     * @param string $configFile
     */
    public function __construct($configFile)
    {
        $this->configFile = $configFile;
    }

    /**
     * Parse
     *
     * @return ParsedPage[]
     */
    public function parse()
    {
        $config = $this->processConfigFile();

        $items = $config[self::TAG_ITEMS];

        $result = $this->parseItems($items);

        return $result;
    }

    /**
     * @param array $items
     *
     * @param int   $level
     *
     * @return ParsedPage[]
     */
    protected function parseItems($items, $level = 1)
    {
        $result = [];

        foreach ($items as $key => $item) {
            $result[] = $this->parseItem($item, $key, $level);
        }

        return $result;
    }

    /**
     * @param array   $item
     * @param string  $key
     * @param integer $level
     *
     * @return ParsedPage
     */
    protected function parseItem($item, $key, $level = 1)
    {
        $propertyAccessor = new PropertyAccessor();

        $page = new ParsedPage($key);
        $page->level = $level;

        foreach ($item as $valueKey => $value) {
            if ($valueKey === self::TAG_ITEMS) {
                $page->children = $this->parseItems($value, ++$level);
            } else {
                $propertyAccessor->setValue($page, $valueKey, $value);
            }
        }

        return $page;
    }

    /**
     * @return array
     */
    protected function processConfigFile()
    {
        if (!file_exists($this->configFile)) {
            throw new NotFoundException(sprintf('File %s not found', $this->configFile));
        }

        $config = Yaml::parse($this->configFile);

        return $config;
    }
}

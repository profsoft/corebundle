<?php
namespace Rup\Bundle\CoreBundle\Exception\Workflow;

use FreeAgent\WorkflowBundle\Exception\WorkflowException as BasicWorkflowException;
use Rup\Bundle\CoreBundle\Exception\Exception;

/**
 * Class WorkflowException
 *
 * @package Rup\Bundle\CoreBundle\Exception\Workflow
 */
class WorkflowException extends BasicWorkflowException implements Exception
{

} 
<?php
namespace Rup\Bundle\CoreBundle\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Rup\Bundle\CoreBundle\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements Exception
{

}

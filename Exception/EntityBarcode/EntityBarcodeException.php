<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityBarcode;

use Rup\Bundle\CoreBundle\Exception\RuntimeException;

/**
 * Class EntityBarcodeException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityBarcode
 */
class EntityBarcodeException extends RuntimeException
{

} 
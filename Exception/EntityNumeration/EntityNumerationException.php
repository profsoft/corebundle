<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityNumeration;

use Rup\Bundle\CoreBundle\Exception\RuntimeException;

/**
 * Class EntityNumerationException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityNumeration
 */
class EntityNumerationException extends RuntimeException
{
}

<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityNumeration;

/**
 * Class EntityNumerationNotFoundException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityNumeration
 */
class EntityNumerationNotFoundException extends EntityNumerationException
{
}

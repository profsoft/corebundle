<?php
namespace Rup\Bundle\CoreBundle\Exception\Controller;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;

/**
 * Class UndefinedPropertyException
 */
class UndefinedPropertyException extends InvalidArgumentException
{

}

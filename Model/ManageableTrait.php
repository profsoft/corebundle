<?php
namespace Rup\Bundle\CoreBundle\Model;

use Exception;
use Doctrine\ORM\EntityManager;
use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;
use Rup\Bundle\CoreBundle\Exception\RepositoryException;

/**
 * Class ManageableTrait
 *
 * @package Rup\Bundle\CoreBundle\Model
 */
trait ManageableTrait /** implements ManageableInterface **/
{
    /**
     * @return string
     */
    abstract protected function getEntityName();

    /**
     * @return EntityManager
     */
    abstract protected function getEntityManager();

    /**
     * Creates an empty object instance.
     *
     * @return mixed
     */
    public function create()
    {
        $className = $this->getEntityName();

        return new $className;
    }

    /**
     * Add an object to the repository
     *
     * @param mixed $object
     *
     * @throws RepositoryException
     */
    public function add($object)
    {
        $this->isExpectedObject($object);
        try {
            $this->getEntityManager()->persist($object);
        } catch (Exception $e) {
            throw RepositoryException::unableToAddEntityToRepository($e);
        }
    }

    /**
     * Removes an object from the repository
     *
     * @param mixed $object
     *
     * @throws RepositoryException
     */
    public function remove($object)
    {
        $this->isExpectedObject($object);
        try {
            $this->getEntityManager()->remove($object);
        } catch (Exception $e) {
            throw RepositoryException::unableToRemoveEntityFromRepository($e);
        }
    }

    /**
     * Saves given object
     *
     * @param mixed $object
     *
     * @throws RepositoryException
     */
    public function save($object)
    {
        $this->isExpectedObject($object);

        try {
            $this->getEntityManager()->flush($object);
        } catch (Exception $e) {
            throw RepositoryException::unableToFlushEntity($e);
        }
    }

    /**
     * Checks entity
     *
     * @param $object
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    private function isExpectedObject($object)
    {
        $className = $this->getEntityName();
        if (!is_object($object) || !$object instanceof $className) {
            throw new InvalidArgumentException();
        }

        return true;
    }
} 

<?php
namespace Rup\Bundle\CoreBundle\Model\ExternalEntity;

use Rup\Bundle\CoreBundle\Repository\EntityRepository;

/**
 * Class ExternalEntityRepositoryTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\ExternalEntity
 */
trait ExternalEntityRepositoryTrait
{
    /**
     * @param int $externalId
     *
     * @return ExternalEntityInterface|null
     */
    public function findOneByExternalId($externalId)
    { 
        /** @var EntityRepository $this */

        $qb = $this->createQueryBuilder('e')
            ->where('e.externalId = :externalId')
            ->setParameter('externalId', $externalId)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

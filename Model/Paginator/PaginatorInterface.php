<?php
namespace Rup\Bundle\CoreBundle\Model\Paginator;

use Porpaginas\Result;

/**
 * Interface PaginatorInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\Paginator
 */
interface PaginatorInterface
{
    /**
     * @return Result
     */
    public function findAllForPaginator();
}

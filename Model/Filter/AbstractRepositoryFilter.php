<?php
namespace Rup\Bundle\CoreBundle\Model\Filter;

use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class AbstractRepositoryFilter
 *
 * @package Rup\Bundle\CoreBundle\Model\Filter
 */
abstract class AbstractRepositoryFilter
{
    /**
     * @param Object[] $objectArray
     *
     * @return array|null
     */
    public function getIdArrayFromObjectArray($objectArray)
    {
        $resultArray = array();

        if (count($objectArray)) {
            foreach ($objectArray as $object) {
                $propertyAccessor = PropertyAccess::createPropertyAccessor();
                $resultArray[]    = $propertyAccessor->getValue($object, 'id');
            }
        }

        return $resultArray;
    }
} 
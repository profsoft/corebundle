<?php
namespace Rup\Bundle\CoreBundle\Model\EntityNumeration;

/**
 * Interface EntityNumerationInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\EntityNumeration
 */
interface EntityNumerationInterface
{
    /**
     * @return string
     */
    public function getEntityAlias();

    /**
     * @param int $entityNumber
     */
    public function setEntityNumber($entityNumber);
}

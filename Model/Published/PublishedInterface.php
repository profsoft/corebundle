<?php
namespace Rup\Bundle\CoreBundle\Model\Published;

/**
 * Interface PublishedInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\Published
 */
interface PublishedInterface
{
    /**
     * @param boolean $published
     *
     * @return mixed
     */
    public function setPublished($published);

    /**
     * @return boolean
     */
    public function isPublished();
}
<?php
namespace Rup\Bundle\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class TimestampTransformer
 */
class TimestampTransformer implements DataTransformerInterface
{
    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var string
     */
    protected $time;

    /**
     * @param string $pattern
     * @param string $time
     */
    public function __construct($pattern, $time)
    {
        $this->pattern = $pattern;
        $this->time    = $time;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        return date($this->pattern, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        $dateTime = \DateTime::createFromFormat($this->pattern, $value);

        if ($dateTime) {
            if ($this->time === 'start_day') {
                $dateTime->setTime(0, 0, 0);
            } elseif ($this->time === 'end_day') {
                $dateTime->setTime(23, 59, 59);
            }

            return $dateTime->getTimestamp();
        }

        return null;
    }
}

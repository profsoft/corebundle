<?php
namespace Rup\Bundle\CoreBundle\Form\DataTransformer\JqueryAutoComplete;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class EntityToEntityIdTransformer
 */
class AutoCompleteTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var
     */
    protected $exactSearch;

    /**
     * @var string
     */
    protected $property;

    /**
     * @var string
     */
    protected $idProperty;

    /**
     * @var string
     */
    protected $transformProperty;

    /**
     * @var bool
     */
    protected $isCreateNewEntity;

    /**
     * @param ObjectManager $em
     * @param string $class
     * @param string $model
     * @param string $property
     * @param string $idProperty
     * @param string $transformProperty
     * @param boolean $isCreateNewEntity
     */
    public function __construct($em, $class, $model, $property, $idProperty, $transformProperty, $isCreateNewEntity)
    {
        $this->em = $em;
        $this->class = $class;
        $this->model = $model;
        $this->property = $property;
        $this->idProperty = $idProperty;
        $this->transformProperty = $transformProperty;
        $this->isCreateNewEntity = $isCreateNewEntity;
    }

    /**
     * Transforms entity into entity id
     *
     * @param mixed $data
     *
     * @throws TransformationFailedException
     *
     * @return object|null
     */
    public function transform($data)
    {
        if ($data) {
            if (is_numeric($data)) {
                $data = intval($data);
                $data = $this->em->getRepository($this->class)->find($data);
            }

            if (is_object($data)) {
                /*if (!$this->em->contains($data)) {
                    throw new TransformationFailedException('Entity passed to the autocomplete must be managed. Do not save the entity at session.');
                }*/
                $propertyAccessor = PropertyAccess::createPropertyAccessor();

                return array(
                    $this->idProperty => $propertyAccessor->getValue($data, $this->idProperty),
                    $this->property => $propertyAccessor->getValue($data, $this->property)
                );
            } else {
                return array($this->idProperty => null, $this->property => $data);
            }
        }

        return null;
    }

    /**
     * Transforms an data into an entity or return string.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function reverseTransform($data)
    {
        if (!$data) {
            return null;
        }

        if ($this->model) {
            return $this->createModel($data);
        }

        if ($this->transformProperty === null) {
            return $this->findEntityByIdProperty($data);
        }

        if ($data[$this->transformProperty]) {
            return $data[$this->transformProperty];
        }

        if ($data[$this->idProperty]) {
            return $data[$this->idProperty];
        }

        if ($data[$this->property]) {
            return $data[$this->property];
        }

        return null;
    }

    /**
     * Finds entity by id property
     *
     * @param $data
     *
     * @return null|object
     */
    protected function findEntityByIdProperty($data)
    {
        if ($identity = $data[$this->idProperty]) {
            $entity = $this->em->getRepository($this->class)->find($identity);

            if ($entity) {
                return $entity;
            }
        } elseif ($this->isCreateNewEntity) {
            $entity = new $this->class();
            $propertyAccessor = PropertyAccess::createPropertyAccessor();

            $propertyAccessor->setValue($entity, $this->property, $data[$this->property]);

            return $entity;
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function createModel($data)
    {
        $model = new $this->model();

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $propertyAccessor->setValue($model, $this->idProperty, $data[$this->idProperty]);
        $propertyAccessor->setValue($model, $this->property, $data[$this->property]);

        return $model;
    }
}
<?php
namespace Rup\Bundle\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class FormPass
 *
 * @package Rup\Bundle\CoreBundle\DependencyInjection\Compiler
 */
class FormPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');

        $resources[] = 'RupCoreBundle::form_fields.html.twig';

        $container->setParameter('twig.form.resources', $resources);
    }
}
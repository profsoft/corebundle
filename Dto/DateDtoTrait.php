<?php
namespace Rup\Bundle\CoreBundle\Dto;

/**
 * Class DateDtoTrait
 *
 * @package Rup\Bundle\CoreBundle\Dto
 */
trait DateDtoTrait
{
    use PublishedDtoTrait;

    /**
     * @var int $createDate
     */
    public $createDate;

    /**
     * @var int $publishDate
     */
    public $publishDate;

    /**
     * @var int $cancelPublishDate
     */
    public $cancelPublishDate;
}
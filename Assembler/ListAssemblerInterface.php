<?php
namespace Rup\Bundle\CoreBundle\Assembler;

use Doctrine\ORM\EntityRepository;

/**
 * Interface ListAssemblerInterface
 *
 * @package Rup\Bundle\CoreBundle\Assembler
 */
interface ListAssemblerInterface
{
    /**
     * @return EntityRepository
     */
    public function getRepository();
}
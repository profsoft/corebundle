<?php
namespace Rup\Bundle\CoreBundle\Command;

use Rup\Bundle\CoreBundle\Model\EntityNumeration\BaseEntityNumerationRepository;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Class AbstractResetEntityNumerationCommand
 *
 * @package Rup\Bundle\CoreBundle\Command
 */
abstract class AbstractResetEntityNumerationCommand extends AbstractCommand
{
    /**
     * @var string
     */
    protected $commonErrorMessage = "Reset process was failed";

    protected function configure()
    {
        $this->setName('rup:reset-entity-numeration');
        $this->setDescription('Resets existing entity numeration.');
        $this->addOption(
            'all',
            null,
            InputOption::VALUE_NONE,
            'If set, all existing entity numerations will be reset.'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return string
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if (!$input->getOption('all')) {
                /** @var QuestionHelper $dialog */
                $questionHelper = $this->getHelper('question');

                $question = new ChoiceQuestion(
                    $this->getQuestion('Please enter the entity name which numeration you want to reset', null),
                    $this->getRepository()->getExistingEntityNames()
                );

                do {
                    $entityName = $questionHelper->ask($input, $output, $question);
                } while (!$entityName);

                $this->getRepository()->resetEntityNumeration($entityName);

                $this->info($output, "Numeration for entity \"{$entityName}\" has been reset successfully.");
            } else {
                $this->getRepository()->resetAllEntityNumerations();

                $this->info($output, 'All existing entity numerations has been reset successfully.');
            }
        } catch (\Exception $exception) {
            $this->error($output, $this->commonErrorMessage);
            $this->error($output, $exception->getMessage());
        }
    }

    /**
     * @return BaseEntityNumerationRepository
     */
    abstract protected function getRepository();
}
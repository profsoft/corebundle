<?php

namespace Rup\Bundle\CoreBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Rup\Bundle\CoreBundle\DependencyInjection\Compiler\JMSSerializerRequirePass;
use Rup\Bundle\CoreBundle\DependencyInjection\Compiler\FormPass;
use Rup\Bundle\CoreBundle\DependencyInjection\Compiler\OverridePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RupCoreBundle
 *
 * @package Rup\Bundle\CoreBundle
 */
class RupCoreBundle extends Bundle
{
    /**
     * @inheritdoc
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $this->addRegisterMappingsPass($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    private function addRegisterMappingsPass(ContainerBuilder $container)
    {
        $mappings = array(
            realpath(__DIR__ . '/Resources/config/doctrine/') => 'Rup\Bundle\CoreBundle\Model',
        );

        $container->addCompilerPass(
            DoctrineOrmMappingsPass::createYamlMappingDriver($mappings)
        );

        $container->addCompilerPass(new FormPass());
        $container->addCompilerPass(new OverridePass());
        $container->addCompilerPass(new JMSSerializerRequirePass());
    }
}

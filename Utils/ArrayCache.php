<?php
namespace Rup\Bundle\CoreBundle\Utils;

use BadMethodCallException;

/**
 * Class ArrayCache
 *
 * @package Rup\Bundle\CoreBundle\Utils
 */
class ArrayCache
{
    /**
     * @var array
     */
    protected $cache;

    /**
     * @var string
     */
    protected $key;

    /**
     * @param string $keyProperty
     */
    public function __construct($keyProperty = 'id')
    {
        $this->cache = array();
        $this->key   = $keyProperty;
    }

    /**
     * Returns object in the cache
     *
     * @param mixed $object
     *
     * @return mixed|null
     */
    public function fetch($object)
    {
        $className = get_class($object);

        if ($this->has($object)) {
            $key = $this->getObjectKey($object);

            return $this->cache[$className][$key];
        }

        return null;
    }

    /**
     * Adds external entity to the local cache
     *
     * @param mixed $object
     */
    public function push($object)
    {
        $className = get_class($object);
        $key       = $this->getObjectKey($object);

        $this->cache[$className][$key] = $object;
    }

    /**
     * Removes object from cache
     *
     * @param mixed $object
     */
    public function remove($object)
    {
        $className = get_class($object);
        $key       = $this->getObjectKey($object);

        if ($this->has($object)) {
            unset($this->cache[$className][$key]);
        }
    }

    /**
     * Returns if object contains in cache
     *
     * @param mixed $object
     *
     * @return bool
     */
    public function has($object)
    {
        $className = get_class($object);

        return $this->getObjectKey($object) !== null &&
            array_key_exists($className, $this->cache) &&
            array_key_exists($this->getObjectKey($object), $this->cache[$className]);
    }

    /**
     * Clears object cache
     *
     * @param string $className
     */
    public function clear($className = null)
    {
        if ($className === null) {
            unset($this->cache[$className]);

            return;
        }

        unset($this->cache);
        $this->cache = array();
    }

    /**
     * @param mixed $object
     */
    protected function getObjectKey($object)
    {
        $keyGetter = 'get' . StringUtils::camelize($this->key);

        if (!method_exists($object, $keyGetter)) {
            throw new BadMethodCallException(
                sprintf('Method %s not found in class %s.', $keyGetter, get_class($object))
            );
        }

        return $object->$keyGetter();
    }
}

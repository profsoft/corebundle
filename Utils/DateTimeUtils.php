<?php
namespace Rup\Bundle\CoreBundle\Utils;

/**
 * Class DateTimeUtils
 */
class DateTimeUtils
{
    /**
     * Convert datetime into D - days, H - hours, M - minutes
     *
     * @param integer $unixtime
     *
     * @return string
     */
    public static function timeToDHM($unixtime)
    {
        $str = '';
        if ($unixtime > 86400) {
            $d = floor($unixtime / 86400);
            $unixtime = $unixtime % 86400;
            $str = $d . ' д ';
        }
        $h = floor($unixtime / 3600);
        $m = round($unixtime % 3600 / 60);
        $str .= $h . ' ч ' . $m . ' м';

        return $str;
    }

    /**
     * Makes time at the end of day
     *
     * @param integer $time
     *
     * @return integer $time
     */
    public static function makeEndOfDay($time)
    {
        $time = strtotime("23:59:59", $time);

        return $time;
    }

    /**
     * Makes time at the end of day
     *
     * @param integer $time
     * @param string $offsetDirection
     * @param mixed $offset
     *
     * @return integer $time
     */
    public static function makeBeginOfDay($time, $offset = null, $offsetDirection = '+')
    {
        $time = strtotime(date('d-m-Y', $time));

        if ($offset && preg_match('/^[0-9]+$/', trim($offset)) && $offsetDirection == '+' || $offsetDirection == '-') {
            $time = strtotime("{$offsetDirection}{$offset} days", $time);
        }

        return $time;
    }

    /**
     * Convert timestamp into date format "d.m.Y"
     *
     * @param integer $time
     *
     * @return string
     */
    public static function timeToDate($time)
    {
        return date('d.m.Y', $time);
    }

    /**
     * Creates period for today
     *
     * @return array
     */
    public static function createTodayPeriod()
    {
        return array(
            self::makeBeginOfDay(time()),
            self::makeEndOfDay(time())
        );
    }

    /**
     * Creates period for current week
     *
     * @return array
     */
    public static function createCurrentWeekPeriod()
    {
        return array(
            self::makeBeginOfDay(strtotime("last monday")),
            self::makeEndOfDay(strtotime("this sunday"))
        );
    }

    /**
     * Creates period for current month
     *
     * @return array
     */
    public static function createCurrentMonthPeriod()
    {
        return array(
            mktime(0, 0, 0, date("m"), 1, date("Y")),
            mktime(0, 0, 0, date("m")+1, 0, date("Y")),
        );
    }

    /**
     * Creates period for current month
     *
     * @return array
     */
    public static function createCurrentYearPeriod()
    {
        return array(
            mktime(0, 0, 0, 1, 1, date("Y")),
            mktime(23, 59, 59, 13, 0, date("Y")),
        );
    }
}

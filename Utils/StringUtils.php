<?php
namespace Rup\Bundle\CoreBundle\Utils;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;
use Rup\Bundle\CoreBundle\Exception\Type\TypeException;

/**
 * Class StringUtils
 *
 * @package Rup\Bundle\CoreBundle\Utils
 */
class StringUtils
{
    /**
     * Converts first character of string to upper case, and other ones to lower case
     *
     * @param string $string
     *
     * @return string
     */
    public static function capitalize($string)
    {
        return mb_convert_case(strtolower($string), MB_CASE_TITLE, 'UTF-8');
    }

    /**
     * Camelizes a given string.
     *
     * @param string $string Some string
     *
     * @return string The camelized version of the string
     */
    public static function camelize($string)
    {
        return strtr(ucwords(strtr($string, array('_' => ' '))), array(' ' => ''));
    }

    /**
     * Splits string to array of words by delimiter, remove blank words
     *
     * @param string $string
     * @param string $delimiter
     * @param int $limit
     * @param string $trimCharsMask
     *
     * @throws TypeException
     * @return array
     */
    public static function splitWords($string, $delimiter = ' ', $limit = null, $trimCharsMask = " \t\n\r\0\x0B")
    {
        if (!is_string($string)) {
            throw new InvalidArgumentException(sprintf("Given argument is not string in %s", __METHOD__));
        }

        if ($limit) {
            $words = explode($delimiter, $string, $limit);
        } else {
            $words = explode($delimiter, $string);
        }

        return array_values(
            array_filter($words, function($word) use ($trimCharsMask) {
                return !empty(trim($word, $trimCharsMask));
            })
        );
    }
}

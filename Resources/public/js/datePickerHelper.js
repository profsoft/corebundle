/**
 * Helper to add date picker selector for fields 
 * 
 * @param string mainInputId selector for input
 * @param string dependantInputId selector for input, optional
 * @constructor
 */
function DatePickerHelper(mainInputId, dependantInputId, options)
{
    var self = this;

    this.defaults = {
        minDate: null,
        dateFormat: "dd.mm.yy"
    };

    this.init(options);

    this.$mainInput = $(mainInputId);
    this.$dependantInput = null;
    
    this.clearSelection(this.$mainInput);
    this.setHandlerOnSiblingButton(this.$mainInput);

    if(dependantInputId == undefined) {
        this.$mainInput.datepicker({dateFormat: self.dateFormat, changeYear: true, changeMonth: true});
    } else {
        this.$dependantInput = $(dependantInputId);

        this.setMainInputDatepicker();
        this.setDependantInputDatepicker();
        this.clearSelection(this.$dependantInput);
        this.setHandlerOnSiblingButton(this.$dependantInput);
    }
}

DatePickerHelper.prototype = {
    init : function(options) {
        for (var name in this.defaults) {
            this[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : this.defaults[name];
        }
    },

    setMainInputDatepicker: function() {
        var self = this;
        this.$mainInput.datepicker("destroy").datepicker({
            dateFormat: self.dateFormat,
            changeYear: true,
            changeMonth: true,
            minDate: self.minDate,
            maxDate: this.$dependantInput.val() != '' ? this.$dependantInput.val() : null,
            onSelect: function() {
                self.setDependantInputDatepicker();
            }
        });
    },

    setDependantInputDatepicker: function() {
        var self = this;
        this.$dependantInput.datepicker("destroy").datepicker({
            dateFormat: self.dateFormat,
            changeYear: true,
            changeMonth: true,
            minDate: this.$mainInput.val() != '' ? this.$mainInput.val() : self.minDate,
            onSelect: function() {
                self.setMainInputDatepicker();
            }
        });
    },

    clearSelection: function($input) {
        $input.on('keydown', function(e) {
            e.preventDefault();
        })
        .on('select', function() {
            $(this).val($(this).val());
        });
    },

    setHandlerOnSiblingButton: function($input) {
        $input.siblings("button").on('click', function (e) {
            e.preventDefault();
            $input.datepicker("show");
        });
    }
}

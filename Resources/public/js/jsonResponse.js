function JsonResponse(object) {
    this.data = null;

    this.parseData(object);
}

JsonResponse.prototype = {
    parseData : function(data) {
        if (typeof (data) == 'string') {
            try {
                data = $.parseJSON(data);
            } catch (e) {
                console.log('invalid data returned: ' + data);
            }
        }

        for ( var key in data) {
            this[key] = data[key];
        }
    },

    getData : function() {
        return this.data;
    }
};

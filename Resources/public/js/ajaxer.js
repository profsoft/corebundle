function Ajaxer(options)
{
    var self = this;

    this.defaults = {
        url: null,
        data: null,
        method: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        processData: true,
        sendOnInit: true,
        showLoader: false,
        hideLoader: false,
        beforeSend: function(xhr) {},
        success: function(data) {
            var jsonResponse = new JsonResponse(data);

            if (jsonResponse.status == 'success' && jsonResponse.type == 'message') {
                self.onSuccess(jsonResponse.data);
            } else if (jsonResponse.status == 'success' && jsonResponse.type == 'html') {
                self.onSuccessHtml(jsonResponse.data);
            } else if (jsonResponse.status == 'error' && (jsonResponse.type == 'message' || jsonResponse.type == 'html')) {
                self.onError(jsonResponse.data);
            } else {
                console.log(jsonResponse);
            }
        },
        complete: function(xhr) {},
        error: function(xhr, ajaxOptions, thrownError) {
            var response = new JsonResponse(xhr.responseText);

            $(document).trigger({
                type: 'AjaxErrorResponse',
                xhr: xhr,
                ajaxOptions: ajaxOptions,
                thrownError: thrownError,
                response: response
            });
        },
        onSuccess: function(data) {},
        onSuccessHtml: function(data) {},
        onError: function(data) {
            document.flashMessages.addMessage(data);
        }
    };

    init(options);

    function init(options) {
        for (var name in self.defaults) {
            self[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : self.defaults[name];
        }
    }

    if (this.sendOnInit) {
        this.send();
    }
}

Ajaxer.prototype = {
    send: function() {
        var self = this;

        $.ajax({
            url: self.url,
            data: self.data,
            method: self.method,
            contentType: self.contentType,
            processData: self.processData,
            beforeSend: function (xhr) {
                if (self.showLoader) {
                    document.modalLoader.show();
                }
                self.beforeSend(xhr)
            },
            success: function(data) {
                if (self.hideLoader) {
                    document.modalLoader.hide();
                }
                self.success(data);
            },
            complete: function(xhr) {
                self.complete(xhr);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                if (self.hideLoader) {
                    document.modalLoader.hide();
                }
                self.error(xhr, ajaxOptions, thrownError);
            }
        });
    }
};

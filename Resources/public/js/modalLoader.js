function ModalLoader()
{
    this.$overlay = null;

    this.builder();
}

ModalLoader.prototype = {

    builder: function() {
        $('body').append('<div class="modal-loader"></div>');
        this.$overlay = $('.modal-loader').hide();
    },

    show: function() {
        var self = this;

        setTimeout(function() {
            self.$overlay.fadeIn();
        }, 10);
    },

    hide: function() {
        this.$overlay.fadeOut();
    }

};
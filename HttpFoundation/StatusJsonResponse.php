<?php
namespace Rup\Bundle\CoreBundle\HttpFoundation;

use Rup\Bundle\CoreBundle\Utils\JsonUtils;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class used for json responses
 */
class StatusJsonResponse extends Response
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR   = 'error';

    const TYPE_MESSAGE   = 'message';
    const TYPE_HTML     = 'html';
    const TYPE_OBJECT     = 'object';
    const TYPE_REDIRECT = 'redirect';

    /**
     * @var string response status
     */
    protected $status;

    /**
     * @var string response type
     */
    protected $type;

    /**
     * @var mixed response data
     */
    protected $data;

    /**
     * JSON response class
     *
     * @param string $status 'success' or 'error' string
     * @param string $type   response type
     * @param mixed  $data   data to send at response
     */
    public function __construct($status, $type, $data = null)
    {
        $this->status = $status;
        $this->type   = $type;
        $this->data   = $data;

        parent::__construct($this->toJson($status, $type, $data), 200);
    }

    /**
     * @param mixed $data
     *
     * @return StatusJsonResponse
     */
    public static function success($data = null)
    {
        return new self(self::STATUS_SUCCESS, self::TYPE_MESSAGE, $data);
    }

    /**
     * @param mixed $data
     *
     * @return StatusJsonResponse
     */
    public static function error($data = null)
    {
        return new self(self::STATUS_ERROR, self::TYPE_MESSAGE, $data);
    }

    /**
     * @param mixed $data
     *
     * @return StatusJsonResponse
     */
    public static function redirect($data = null)
    {
        return new self(self::STATUS_SUCCESS, self::TYPE_REDIRECT, $data);
    }

    /**
     * @param mixed $data
     *
     * @return StatusJsonResponse
     */
    public static function formSuccess($data = null)
    {
        return new self(self::STATUS_SUCCESS, self::TYPE_HTML, $data);
    }

    /**
     * @param mixed $data
     *
     * @return StatusJsonResponse
     */
    public static function formError($data = null)
    {
        return new self(self::STATUS_ERROR, self::TYPE_HTML, $data);
    }

    /**
     * Returns json representation of given data
     *
     * @param string $status
     * @param string $type
     * @param mixed  $data
     *
     * @return string
     */
    public function toJson($status, $type, $data)
    {
        return json_encode($this->toArray($status, $type, $data));
    }

    /**
     * Converts given data to array
     *
     * @param string $status
     * @param string $type
     * @param mixed  $data
     *
     * @return array
     */
    protected function toArray($status, $type, $data)
    {
        $result = array();

        $result['status'] = $status;
        $result['type']   = $type;
        $result['data'] = JsonUtils::prepare($data);

        return $result;
    }
} 